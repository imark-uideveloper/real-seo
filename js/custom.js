jQuery(function (jQuery) {

    // Preloader //

    var preloader = jQuery('.preloader');
    jQuery(window).load(function () {
        preloader.remove();
    });

    // Gallery //

    var filterList = {
        init: function () {
            // MixItUp plugin
            // http://mixitup.io
            jQuery('#portfoliolist').mixItUp({
                selectors: {
                    target: '.portfolio',
                    filter: '.filter'
                },
                load: {
                    filter: '.seo'
                }
            });
        }
    };
    // Run the show!
    filterList.init();

    // Client Logo Slider //

    $('.client-logo-slider').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1000,
        prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
        nextArrow: '<a class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
        responsive: [
            {
                breakpoint: 1499,
                settings: {
                    arrows: false,
                    slidesToShow: 5
                }
         },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    slidesToShow: 2
                }
         },
            {
                breakpoint: 568,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
         }
      ]
    });
});

/**** Textarea First Letter Capital ****/
jQuery('textarea.form_control1,textarea.form_control2,textarea.form_control3').on('keypress', function (event) {

    var $this = jQuery(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});

////////////////////////////////////////////////////////////////////////////////////////////////////


jQuery('.cs-btn').append("<span class='lines top'></span><span class='lines right'></span><span class='lines bottom'></span><span class='lines left'></span>");
jQuery('.cs-btn').attr('pk', '');
////////////////////////////////////////////////////////////////////////////////////////////////////

// grab the initial top offset of the navigation 
var stickyNavTop = $('body').offset().top;
// our function that decides weather the navigation bar should have "fixed" css position or not.
function stickyNav() {
    var scrollTop = $(window).scrollTop(); // our current vertical position from the top

    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
    // otherwise change it back to relative
    if (scrollTop > 30) {
        $('header').addClass('sticky animated fadeInDown');
    } else {
        $('header').removeClass('sticky fadeInDown');
    }

};

stickyNav();
// and run it again every time you scroll
$(window).scroll(function () {
    stickyNav();
});

///////////////////////////////////////////////////////////////////////////////////////////////
var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true, // act on asynchronously loaded content (default is true)
    callback: function (box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
});
wow.init();
/////////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {
    var bigimage = $("#big");
    var thumbs = $("#thumbs");
    var totalslides = 10;
    var syncedSecondary = true;

    bigimage.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: false,
        autoplay: true,
        dots: false,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
    }).on('changed.owl.carousel', syncPosition);

    thumbs
        .on('initialized.owl.carousel', function () {
            thumbs.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: 3,
            dots: true,
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: totalslides,
            responsiveRefreshRate: 100
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        //if loop is set to false, then you have to uncomment the next line
        //var current = el.item.index;

        //to disable loop, comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);

        if (current < 0) {
            current = count;
        }
        if (current > count)  {
            current = 0;
        }
        //to this
        thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbs.find('.owl-item.active').length - 1;
        var start = thumbs.find('.owl-item.active').first().index();
        var end = thumbs.find('.owl-item.active').last().index();

        if (current > end) {
            thumbs.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            thumbs.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            bigimage.data('owl.carousel').to(number, 100, true);
        }
    }

    thumbs.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        bigimage.data('owl.carousel').to(number, 300, true);
    });

});
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('.firstCap').on('keypress', function (event) {
    var $this = $(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    $(this).val(FLC + con);
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {
    $(".caret").click(function () {
        $(".submenu").toggle();
    });
});
